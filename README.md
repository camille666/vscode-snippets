# WPT Snippets

> vscode 代码补全插件，帮助你急速编写业务代码

![install](https://cdn.weipaitang.com/static/20190828a47ff261-cc3e-476d-8eff-5b7a499645e0-W1596H350/w/640)

## 代码补全列表

### WPT.

* showPage => ``` WPT.showPage('$1');" ```
* redirect => ``` WPT.redirect('$1');" ```
* nowTime => ``` WPT.nowTime" ```
* API => ``` const API = WPT.Util.handleApi({ ... " ```
* getData => ``` WPT.getData(API.$1, {}, (res) => { ... " ```

### WPT.Modal.

* tips => ``` WPT.Modal.tips(${1:res.msg}); ```
* alert => ``` WPT.Modal.alert(${1:res.msg}); ```
* open => ``` WPT.Modal.open(${1:<Components />}, ${2:options}); ```
* close => ``` WPT.Modal.close(); ```

### WPT.Util.

* handleApi => ``` WPT.Util.handleApi({ ... ```
* query => ``` WPT.Util.query(); ```
* formatDate => ``` WPT.Util.formatDate('${1:y-m-d H:i:s}', ${2:timestamp}); ```

## TODO

* 着重结合[WPT文档](http://webapp.jsdoc.wpt.la/index.html)进行补全
* 引入组件库wptd的补全

欢迎意见和pr

## 关于 vscode snippets

This is the source code for [Snippet Guide](https://code.visualstudio.com/api/language-extensions/snippet-guide).

### VS Code API

### Contribution Points

- [`contributes.snippets`](https://code.visualstudio.com/api/references/contribution-points#contributes.snippets)

### Running the Sample

- Run the `Run Extension` target in the Debug View
- When you type `log` in a JavaScript file, you would see the snippet `Print to console`.